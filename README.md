# MyNomNom

MyNomNom est un logiciel de gestionnaire de recette de cuisine libre et modulable 

Il s'agit d'une application GUI codée à l'aide du framework JavaFX

Les données doivent être stockées dans des fichiers, stockés dans une base de données SQL. (à voir: Base de donnée objet)

## Features

- [ ] L'utilisateur peut créer une recette en y ajoutant les ingredients et la quantité
- [ ] Les recettes peuvent être modifiées après avoir été enregistrées
- [ ] les recettes peuvent être clonée puis modifiées pour des ajustements 
- [ ] Les recettes peuvent être supprimées
- [ ] Un moteur de recherche permet de chercher une recette grâce à son nom mais aussi grâce aux ingrédients
- [ ] Le moteur de recherche permet de trouver des recettes avec un système de pertinance en fonction des ingrédients que l'on veut cuisiner
- [ ] Les ingrédient ont une pondération pour permettre un resultat de recherche plus pertinent
- [ ] possibilité de recherche par appréciation du plat
- [ ] Création de "menu"  qui regroupent des plats 
- [ ] possibilité de trier les menus par saison 
- [ ] Calculateur de macro-nutriments
- [ ] calculateur de quantité automatique
- [ ] avertissement en cas de recette non diététique


